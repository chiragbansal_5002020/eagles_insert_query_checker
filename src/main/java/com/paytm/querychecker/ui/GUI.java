package com.paytm.querychecker.ui;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.AttributeSet;
import javax.swing.text.DefaultCaret;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import com.paytm.utils.DBFunctions;
import com.paytm.utils.Query_Validation_Runner;

public class GUI extends JFrame{
	
	JPanel pnl_main;
	JPanel pnl_fileSelectionHolder;
	JTextField tf_fileSelection;
	JButton btn_chooseFile;
	JPanel pnl_messagePane;
	JTextArea txt_message;
	JButton btn_Run;
	String filePath;
	JPanel pnl_run;
	JMenuBar menuBar;
	JMenu menu;
	
	public GUI() {
		pnl_main = new JPanel();
		
		tf_fileSelection = new JTextField(20);
		btn_chooseFile = new JButton("Select File");
		
		pnl_fileSelectionHolder = new JPanel();
		pnl_fileSelectionHolder.add(tf_fileSelection);
		pnl_fileSelectionHolder.add(btn_chooseFile);
		
		pnl_messagePane = new JPanel();
		txt_message = new JTextArea(20,30);
		JScrollPane sPane = new JScrollPane(txt_message);
		sPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		sPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pnl_messagePane.add(sPane);
		txt_message.setLineWrap(true);
		txt_message.setEditable(false);
		txt_message.setWrapStyleWord(true);
		DefaultCaret caret = (DefaultCaret) txt_message.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		txt_message.setText("Select the file and click Run button");
		
		pnl_run = new JPanel();
		btn_Run = new JButton("Run");
		pnl_run.add(btn_Run);
		btn_Run.setEnabled(false);
		
		
		pnl_main = new JPanel();
		pnl_main.setLayout(new BoxLayout(pnl_main, BoxLayout.Y_AXIS));
		pnl_main.add(pnl_fileSelectionHolder);
		pnl_main.add(pnl_messagePane);
		pnl_main.add(pnl_run);
	
		
		btn_chooseFile.addActionListener(e->{
			JFileChooser jfc = 
					new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
			 
			 FileNameExtensionFilter restrict = new FileNameExtensionFilter("Only excel files", "xls","xlsx"); 
	         jfc.setFileFilter(restrict); 
			
	         int r = jfc.showOpenDialog(null); 
	         if (r == JFileChooser.APPROVE_OPTION) { 
	                File file = jfc.getSelectedFile();  
	                String path = file.getAbsolutePath().trim();
	                if(!path.isEmpty()) {
	                	System.out.println("File Selected : "+ path);
	                	this.filePath = path;
	                	tf_fileSelection.setText(path);
	                	btn_Run.setEnabled(true);
	                }
	         
	            } 
			 
		});
		
		btn_Run.addActionListener(e->{
			File file = new File(System.getProperty("user.dir")+"/settings.properties");
			if(!file.exists()) {
				showMessage("Database settings not found !. Go to Window > Settings menu");
				
			}else {
				Properties props = new Properties();
				try {
					props.load(new FileInputStream(new File("./settings.properties")));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				writeToStatusArea("Database Connection Parameters  ");
				writeToStatusArea(props.getProperty("dbIP"));
				writeToStatusArea(props.getProperty("dbPort"));
				writeToStatusArea(props.getProperty("username"));
				writeToStatusArea(props.getProperty("password"));
				writeToStatusArea(props.getProperty("dbName"));
				
				System.setProperty("databaseIP", props.getProperty("dbIP"));
				System.setProperty("dbPortNumber", props.getProperty("dbPort"));
				System.setProperty("userName", props.getProperty("username"));
				System.setProperty("password", props.getProperty("password"));
				System.setProperty("dbName", props.getProperty("dbName"));
				
				
				try {
					if(!new DBFunctions().isDBConnectionSuccess(props.getProperty("dbName"))) {
						writeToStatusArea("Connection to DB was not successfull !\n"
								+ "Please make sure VPN is connected and DB settings are correct");
					}else {
						new Query_Validation_Runner(this).start(filePath);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			
		});
		
		menuBar = new JMenuBar();
		menu = new JMenu("Window");
		JMenuItem item_settings = new JMenuItem("settings");
		item_settings.addActionListener(e->{
			new SettingsPage(this);
		});
		JMenuItem item_exit = new JMenuItem("Exit");
		item_exit.setMnemonic(KeyEvent.VK_E);
		item_exit.addActionListener((event) -> System.exit(0));
		menu.add(item_settings);
		menu.add(item_exit);
		menuBar.add(menu);
		
		
		setJMenuBar(menuBar);
		getContentPane().add(pnl_main);
		setSize(400,450);
		setResizable(false);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void writeToStatusArea(String text) {
		this.txt_message.append("\n"+text);
	}
	
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(this, message);
	}
	
	private void appendToPane(JTextPane tp, String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
	
	public static void main(String[] args) {
		new GUI();
	}
}
