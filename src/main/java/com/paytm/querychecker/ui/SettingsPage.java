package com.paytm.querychecker.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SettingsPage{
	JFrame window ;
	JFrame parent;
	JLabel lbl_dbIP =               new JLabel("Database IP   ");
	JLabel lbl_dbPORT =             new JLabel("Database PORT ");
	JLabel lbl_UserName = 			new JLabel("Username      ");
	JLabel lbl_password = 			new JLabel("Password      ");
	JLabel lbl_dbName =   			new JLabel("Database  Name");
	JLabel lbl_numberOfConnections= new JLabel("DB Connections");
	JTextField tf_dbIP = new JTextField(15);
	JTextField tf_dbPort = new JTextField(15);
	JTextField tf_dbUsername = new JTextField(15);
	JTextField tf_dbPassword = new JTextField(15);
	JTextField tf_dbName = new JTextField(15);
	JTextField tf_dbCon = new JTextField(15);
	JButton btn_Save = new JButton("Save");
	JPanel pnl_main = new JPanel();
	JPanel pnl_save = new JPanel();
	
	public SettingsPage(JFrame parent) {
		File file = new File("./settings.properties");
		window = new JFrame("Settings");
		if(!file.exists()) {
			try {
				file.createNewFile();
				Properties props = new Properties();
				props.setProperty("dbIP", "");
				props.setProperty("dbPort", "");
				props.setProperty("username", "");
				props.setProperty("password", "");
				props.setProperty("dbName", "");
				props.setProperty("connectionsCount", "");
				
				props.store(new FileOutputStream(file), "");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			Properties props = new Properties();
			try {
				props.load(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			tf_dbIP.setText(props.get("dbIP").toString());
			tf_dbPort.setText(props.getProperty("dbPort"));
			tf_dbUsername.setText(props.getProperty("username"));
			tf_dbPassword.setText(props.getProperty("password"));
			tf_dbName.setText(props.getProperty("dbName"));
			tf_dbCon.setText(props.getProperty("connectionsCount"));
			
		}
		
		btn_Save.addActionListener(e->{
			String txt_ip = tf_dbIP.getText().trim();
			String txt_port = tf_dbPort.getText().trim();
			String txt_user = tf_dbUsername.getText().trim();
			String txt_pass = tf_dbPassword.getText().trim();
			String txt_dbname = tf_dbName.getText().trim();
			String txt_con = tf_dbCon.getText().trim();
			
			if(txt_ip.isEmpty() || txt_port.isEmpty() || txt_user.isEmpty() || txt_pass.isEmpty()
					|| txt_dbname.isEmpty() || txt_con.isEmpty()) {
				JOptionPane.showMessageDialog(this.window, "Please provide value for all fields");
			}else {
				Properties props = new Properties();
				props.setProperty("dbIP", txt_ip);
				props.setProperty("dbPort", txt_port);
				props.setProperty("username", txt_user);
				props.setProperty("password", txt_pass);
				props.setProperty("dbName", txt_dbname);
				props.setProperty("connectionsCount", txt_con);
				
				try {
					props.store(new FileOutputStream(file), "");
					JOptionPane.showMessageDialog(this.window, "Settings saved successfully !!");
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		JPanel pnl_1 = new JPanel();
		pnl_1.add(lbl_dbIP);
		pnl_1.add(tf_dbIP);
		
		JPanel pnl_2 = new JPanel();
		pnl_2.add(lbl_dbPORT);
		pnl_2.add(tf_dbPort);
		
		JPanel pnl_3 = new JPanel();
		pnl_3.add(lbl_UserName);
		pnl_3.add(tf_dbUsername);
		
		JPanel pnl_4 = new JPanel();
		pnl_4.add(lbl_password);
		pnl_4.add(tf_dbPassword);
		
		JPanel pnl_5 = new JPanel();
		pnl_5.add(lbl_dbName);
		pnl_5.add(tf_dbName);
		
		JPanel pnl_6 = new JPanel();
		pnl_6.add(lbl_numberOfConnections);
		pnl_6.add(tf_dbCon);
		
		JPanel pnl_7 = new JPanel();
		pnl_7.add(btn_Save);
		
		pnl_save.setLayout(new BoxLayout(pnl_save, BoxLayout.Y_AXIS));
		
		pnl_save.add(pnl_1);
		pnl_save.add(pnl_2);
		pnl_save.add(pnl_3);
		pnl_save.add(pnl_4);
		pnl_save.add(pnl_5);
		pnl_save.add(pnl_6);
		pnl_save.add(pnl_7);
		
		pnl_main.add(pnl_save);
		
		window.getContentPane().add(pnl_main);
		
		window.setSize(400,250);
		window.setLocationRelativeTo(parent);
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}
	
}
