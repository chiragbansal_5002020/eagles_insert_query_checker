package com.paytm.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;	



public class Query_Validation_Runner2{
	
}
//enum Type{
//	XLSX, XLS
//}
//
//public class Query_Validation_Runner2 {
//	
//	public FileInputStream fis = null;
//	private Workbook workbook = null;
//	private Sheet sheet = null;
//	private Type fileType;
//	File resultFile = null;
//	Workbook resultWorkbook = null;
//	Sheet resultSheet = null;
//	FileOutputStream oStream = null;
//	
//	DBFunctions dbFunc;
//	
//	public void start(String filePath) throws IOException {
//		
//		dbFunc = new DBFunctions();
//		resultFile = getNewFile("./result.xlsx");
//		resultWorkbook = new XSSFWorkbook();
//		
//		
//		File file = new File(filePath);
//		fis = new FileInputStream(file);
//		
//		if(filePath.endsWith(".xlsx")) {
//			fileType = Type.XLSX;
//		}else if(filePath.endsWith(".xls")) {
//			fileType = Type.XLS;
//		}
//		
//		
//		if(fileType == Type.XLSX) {
//			workbook = new XSSFWorkbook(fis);
//		}else {
//			workbook = new HSSFWorkbook(fis);
//		}
//		
//		int count =  workbook.getNumberOfSheets();
//		
//		for(int index=0; index<count; index++) {
//			System.out.println("Checking for Sheet at index "+index);
//			sheet = workbook.getSheetAt(index);
//			System.out.println("Sheet Name : "+sheet.getSheetName());
//			Row headerRow = sheet.getRow(sheet.getFirstRowNum());
//			int queryColumnNumber=Integer.MIN_VALUE;
//			int businessRemarksCol = Integer.MIN_VALUE;
//			
//			for(int col=headerRow.getFirstCellNum(); col<=headerRow.getLastCellNum(); col++) {
//				if(getCellData(headerRow,col).toUpperCase().equals("BUSINESS REMARKS")) {
//					businessRemarksCol = col;
//					if(queryColumnNumber!=Integer.MIN_VALUE)
//						break;
//				}
//				if(getCellData(headerRow,col).toUpperCase().equals("SQL QUERY")) {
//					queryColumnNumber = col;
//					if(businessRemarksCol!=Integer.MIN_VALUE)
//						break;
//				}
//			}
//			
//			if(queryColumnNumber==Integer.MIN_VALUE) {
//				System.out.println("Column with name \"SQL Query\" not found in sheet. Skipped this sheet!");
//				continue;
//			}
//			
//			int lastRowNumber = sheet.getLastRowNum();
//			Row row=null;
//			String query="";
//			
//			Sheet resultSheet = resultWorkbook.createSheet(sheet.getSheetName());
//			Row resultHeaderRow = resultSheet.createRow(0);
//			resultHeaderRow.createCell(0).setCellValue("SQL Query");
//			resultHeaderRow.createCell(1).setCellValue("Result");
//			Row resultSheetRow = null;
//			int resultSheetRowIndex=1;
//			
//			for(int rowNum=sheet.getFirstRowNum()+1; rowNum<lastRowNumber; rowNum++) {
//				row = sheet.getRow(rowNum);
//				query = getCellData(row,queryColumnNumber);
//				if(!query.isEmpty() && getCellData(row,businessRemarksCol).trim().equalsIgnoreCase("ok")) {
//					resultSheetRow = resultSheet.createRow(resultSheetRowIndex++);
//					resultSheetRow.createCell(0).setCellValue(query);
//					resultSheetRow.createCell(1).setCellValue(
//						dbFunc.insertUpdateRecord(query, System.getProperty("dbName"))
//					);
//				}
//			}
//		}
//		
//		
//		if(resultWorkbook!=null) {
//			try {
//				oStream = new FileOutputStream(resultFile);
//			} catch (FileNotFoundException e) {
//				System.out.println("Error occured while getting output stream for file : "+filePath);
//				e.printStackTrace();
//			}
//			try {
//				resultWorkbook.write(oStream);
//				oStream.close();
//			} catch (IOException e) {
//				System.out.println("Error occured while writing to output stream / "
//						+ "closing output stream for excel file");
//				e.printStackTrace();
//			}
//
//		}
//		
//		dbFunc.closeAllConnections();
//	}
//	
//	private String getCellData(Row row, int cellNumber) {
//		Cell cell = row.getCell(cellNumber);
//		if(cell==null)
//			return "";
//		String result="";
//		switch (cell.getCellType()) {
//		case Cell.CELL_TYPE_NUMERIC:
//			result = Double.toString(cell.getNumericCellValue());
//			break;
//		case Cell.CELL_TYPE_STRING:
//			result = cell.getStringCellValue();
//			break;
//		case Cell.CELL_TYPE_BOOLEAN:
//			result = Boolean.toString(cell.getBooleanCellValue());
//			break;
//		case Cell.CELL_TYPE_BLANK:
//			result = "";
//			break;
//		case Cell.CELL_TYPE_FORMULA:
//			 switch(cell.getCachedFormulaResultType()) {
//	            case Cell.CELL_TYPE_NUMERIC:
//	                result =  Double.toString(cell.getNumericCellValue());
//	                break;
//	            case Cell.CELL_TYPE_STRING:
//	                result = cell.getRichStringCellValue().getString();
//	                break;
//	        }
//		default:
//			break;
//		}
//		
//		return result;
//	}
//	
//	private File getNewFile(String filePath) {
//		File file = new File(filePath);
//		if(file.exists() && !file.isDirectory()) {
//			String extension = filePath.substring(filePath.lastIndexOf(".")+1);
//			filePath = filePath.substring(0, filePath.lastIndexOf("."+extension));
//			filePath+="_"+Timer.getCurrentTimeStamp()+"."+extension;
//			file = new File(filePath);
//		}
//		return file;
//	}
//	
//	public static void main(String[] args) throws IOException {
//		String path="";
//		if(args.length==0)
//			path = "./data.xlsx";
//		else
//			path = args[0];
//		
//		System.setProperty("databaseIP", "sawslmktdb05");
//		System.setProperty("dbPortNumber", "3306");
//		System.setProperty("userName", "mall_stage");
//		System.setProperty("password", "Stg#M@ll#97#One(");
//		System.setProperty("dbName","catalog_thirdparty");
//		
//		new Query_Validation_Runner2().start(path);
//	}
//	
//}
