package com.paytm.utils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


public class DBFunctions 
{
	private Map<String,Connection> connections = new HashMap<>();
	
	
	private boolean isDBConnectionExist(String dbName) {
		if(connections.get(dbName)==null)
			return false;
		else
			return true;
	}
	
	private Connection getExistingConnectionOrCreateNew(String dbName) throws ClassNotFoundException {
		if(isDBConnectionExist(dbName))
			return connections.get(dbName);
		else {
			Connection con = DBConnection(dbName);
			connections.put(dbName, con);
			return con;
		}
		
	}
	
	public void closeAllConnections() {
		for(String dbName : connections.keySet())
			connectionClose(null, null, connections.get(dbName));
	}
	
	public boolean isDBConnectionSuccess(String dbName) {
		try {
			Connection con = DBConnection(dbName);
			if(con==null)
				return false;
			else {
				closeDBConnection(con);
				return true;
			}
			
		}catch(Exception e) {
			return false;
		}
	}
	
	private Connection DBConnection(String dbName) throws ClassNotFoundException
	{	
		String sDatabaseIP=System.getProperty("databaseIP");
		int iDBPortNumber= Integer.parseInt(System.getProperty("dbPortNumber"));
		String sDatabaseName=dbName;
		String sUserName=System.getProperty("userName");
		String sPassword=System.getProperty("password");
		Connection con = null;
		String url = "jdbc:mysql://" + sDatabaseIP + ":" + iDBPortNumber + "/" + sDatabaseName;
		System.out.println("URL DATABASE CONNECTION:"+ url);
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			con= DriverManager.getConnection(url, sUserName, sPassword);
			System.out.println("Connected successfully: " + con);
			
		} 
		catch (SQLException sq) {
			System.out.println("url : "+ url);
			System.out.println("sUserName : "+ sUserName);
			System.out.println("sPassword : "+ sPassword);
			System.out.println("Connection not made "+ sq);
			sq.printStackTrace();
		}
		return con;
	}

	/**
	 * This function is used to perform select query
	 * 
	 * @param FlagCount is used to get the no. of values from particular table.
	 * @param Query -- just define a string of Query
	 * @param DBsetup -- Used to pass the setup details for connection. Already defined in Common constant
	 * 
	 */

		public String SelectQuery(int FlagCount, String Query, String dbName) throws Exception
		{	
			Connection con = getExistingConnectionOrCreateNew(dbName);
			String ColumnValues = "";
	
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(Query);
			if(!res.isBeforeFirst())
			{
				ColumnValues = "";
			}
	
			else
			{
				while(res.next())
				{
					for(int values = 1; values<=FlagCount;values++)
					{
						try
						{
							ColumnValues = res.getString(values)+";;;"+ColumnValues;
						}
						catch(SQLException e)
						{
							e.printStackTrace();
						}
					}
				}
	
				int lastColonValue = ColumnValues.lastIndexOf(";;;");
				ColumnValues = ColumnValues.substring(0, lastColonValue).trim().toString();
				//ColumnValues = new StringBuilder(ColumnValues).replace(lastColonValue, lastColonValue+1, "").toString();
			}
	
			connectionClose(st, res, null);
			return ColumnValues ;
		}
		
		public LinkedHashMap<String, String> selectQueryHashmap(String Query, String dbName)
		{
			LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
			// Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key 
			
			try{
				Connection con = getExistingConnectionOrCreateNew(dbName);
				Statement st = con.createStatement();
				ResultSet res=st.executeQuery(Query);
				ResultSetMetaData metaData = res.getMetaData();

				int count = metaData.getColumnCount();
				String value="";
				SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				while(res.next())
				{
					for(int i=1;i<=count;i++)
					{
						if(metaData.getColumnTypeName(i).toUpperCase().contains("DATE"))
						{
							if(metaData.getColumnTypeName(i).toUpperCase().equals("DATE"))
							{
								Date date=res.getDate(i);
								//System.out.println("Date equals null "+date==null);
								if(date==null)
									value="0000-00-00";
								else value=date.toString();
							}

							if(metaData.getColumnTypeName(i).toUpperCase().equals("DATETIME"))
							{
								Timestamp ts = res.getTimestamp(i);
								//System.out.println("Date equals null "+date==null);
								if(ts==null)
									value="0000-00-00 00:00:00";
								else {value=datetimeFormat.format(ts);
								//System.out.println(value.toString());
								}
							}

						}else
							value=res.getString(i);

						if(value==null)
							value="null";

						if(res.isFirst())
						{
							map.put(metaData.getColumnLabel(i),value);

						}else{
							map.put(metaData.getColumnLabel(i),map.get(metaData.getColumnLabel(i))+";;;"+value); // in case result set contains multiple rows
						}

					}
				}
				connectionClose(st, res, con);
			}catch(Exception e){
				System.out.println(e.getMessage());
				map.put("Message", e.getMessage());
				return map;
			}

			return map;
		}

	
		public ResultSet selectQueryResultSet(String selectSql, String dbName) throws Exception 
		{
			Connection con = getExistingConnectionOrCreateNew(dbName);
			// execute select SQL statement
			Statement st = con.createStatement();
			ResultSet resultSet = st.executeQuery(selectSql);
			return resultSet;
		}
		
		public void closeDBConnection(Connection con) {
			try {
				if(con!=null)
					con.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		public void connectionClose(Statement st1, ResultSet res, Connection con)
		{
			generic_methods gm = new generic_methods();
			try
			{   
				if(res!=null)
					res.close();
	
				if(st1!=null)
					st1.close();
	
				if(con!=null)
					con.close();
				con = null;
			}
			catch(Exception e)
			{
				System.out.println(gm.getExceptionInfo(e));
			}
		}
		public ArrayList<String> getParticularRecordsUsingExistingConnection(String query,String dbName) throws Exception {
		
			System.out.println("Query particular records: " + query);
			//LOGGER.info("Query particular records: " + query);
			Connection con = getExistingConnectionOrCreateNew(dbName);
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);

			ArrayList<String> arrayList = new ArrayList<String>();
			if (!rs.isBeforeFirst()) {
//				LOGGER.info("Data is not found");
			} else {
				while (rs.next()) {
					arrayList.add(rs.getString(1));
					
				}
			}
			rs.close();
			st.close();

			return arrayList;
		}
		
		public String insertUpdateRecord(String query,String dbName){
			//System.out.println("Query INSERT UPDATE: " + query);
			int x = -1;
			Statement st = null;
			Connection con = null;
			try {
				con = getExistingConnectionOrCreateNew(dbName);
			} catch (Exception e) {
				e.printStackTrace();
				return "Fail -- Connection to Database - "+dbName+" was unsuccessfull!";
			}
			try {
				st = con.createStatement();
				x = st.executeUpdate(query);
				st.close();

				return "Pass  -- "+x+" rows inserted/updated successfully!";
			} catch (SQLException e) {
				//e.printStackTrace();
				return "Fail  -- "+e.getMessage();
			}

		}
		
		public static void main(String[] args) throws Exception {
			System.setProperty("databaseIP", "sawslmktdb01");
			System.setProperty("dbPortNumber", "3306");
			System.setProperty("userName", "mall_stage");
			System.setProperty("password", "Stg#M@ll#97#One(");
			DBFunctions dbfs = new DBFunctions();
			
			System.out.println(dbfs.insertUpdateRecord(
					"insert into category_attribute_value_map (agg_code,agg_category_code,agg_attr_code,agg_attr_val_code,mkt_code,mkt_category_code,mkt_attr_code,mkt_attr_val_code,status) values(\"EAGLE\",\"106113\",\"Metal\",\"Alloy\",\"PAYTMMALL\",\"5063\",\"Base Material\",\"Alloy\",\"1\");",
					"catalog_thirdparty"));
			
		}
}
