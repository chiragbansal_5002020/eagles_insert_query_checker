package com.paytm.queryupdater;

import java.io.File;
import java.io.FileWriter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.paytm.utils.DBFunctions;

public class Test {
	DBFunctions dbfs = new DBFunctions();
	DBFunctions dbfs1 = new DBFunctions();
	DBFunctions dbfs2 = new DBFunctions();
	DBFunctions dbfs3 = new DBFunctions();
	DBFunctions dbfs4 = new DBFunctions();
	
	String keyToAdd = "kid";
	String valueToAdd = "auto_test";
	
	public void doRun() throws Exception {
		
//		for(int count=0; count<10; count++) {
//			String query =
//					"Select id from catalog_product order by id desc limit "+(count*1000)+" , 1";
//			String id = dbfs.SelectQuery(1, query, "mktplace_catalog");
//			System.out.println(id);
//		}
		
		File file = new File("Result_"+System.currentTimeMillis()+".txt");
		FileWriter writer = new FileWriter(file);
		int rows=0;
		
		for(int mainCounter=0; mainCounter<100; mainCounter++) {
			String selectAttributesQuery = 
					"select attributes,id from catalog_product where "
					+" vertical_id = 66 and created_at < '2019-03-31 17:15:59' "
					+ " order by id desc limit "+(mainCounter*100)+" , 100";
			String[] results = dbfs.SelectQuery(2, selectAttributesQuery, "mktplace_catalog").split(";;;");
			
			String id="";
			String attributes="";
			String modifiedAttributes="";
			String updateQuery="";
			String queryResult="";
			for(int i=results.length-1; i>0; i-=2) {
				id = results[i-1];
				attributes = results[i];
				modifiedAttributes = getModifiedAttributesJson(attributes);
				updateQuery = "UPDATE catalog_product SET attributes='"+modifiedAttributes+"' WHERE id = "+id;
				//System.out.println(updateQuery);
				writer.write("\n"+ id);
				queryResult = dbfs.insertUpdateRecord(
						updateQuery,
						"mktplace_catalog");
				writer.write("\n"+queryResult);
				if(queryResult.startsWith("Fail")) {
					writer.write("\n"+updateQuery);
				}
				
				writer.write("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
				//System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
				writer.flush();
				rows++;
				System.out.println("Count -- "+ rows +"  , product id --  "+id);
			}
		}
		
		dbfs.closeAllConnections();
		if(writer!=null)
			writer.close();
	}
	
	
	
	private String getModifiedAttributesJson(String attributes) {
		//System.out.println(attributes);
		JSONParser parser = new JSONParser();
		JSONObject object=null;
		try {
			object = (JSONObject) parser.parse(attributes);
			object.put(keyToAdd, valueToAdd);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return object.toString();
	}
	
	public static void main(String[] args) throws Exception {
		System.setProperty("databaseIP", "sawslmktdb02");
		System.setProperty("dbPortNumber", "3306");
		System.setProperty("userName", "mall_stage");
		System.setProperty("password", "Stg#M@ll#97#One(");
		
		new Test().doRun();
	}
}
